// Razor Alliane Timer GrabDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Razor Alliane Timer Grab.h"
#include "Razor Alliane Timer GrabDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CRazorAllianeTimerGrabDlg dialog




CRazorAllianeTimerGrabDlg::CRazorAllianeTimerGrabDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CRazorAllianeTimerGrabDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	//m_strCookie = "";
	m_mt = NULL;
	m_ts = NULL;
}

CRazorAllianeTimerGrabDlg::~CRazorAllianeTimerGrabDlg()
{
	if (m_mt != NULL){
		delete m_mt;
	}
	if (m_ts != NULL) {
		delete m_ts;
	}
}

void CRazorAllianeTimerGrabDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CRazorAllianeTimerGrabDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_START, &CRazorAllianeTimerGrabDlg::OnBnClickedButtonStart)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_CHECK_REMEMBER, &CRazorAllianeTimerGrabDlg::OnBnClickedCheckRemember)
END_MESSAGE_MAP()


// CRazorAllianeTimerGrabDlg message handlers

BOOL CRazorAllianeTimerGrabDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	m_list_forums = (CListCtrl*)GetDlgItem(IDC_LIST_FORUMS);
	m_list_timers = (CListCtrl*)GetDlgItem(IDC_LIST_TIMERS);
	m_static_con_state = (CStatic*)GetDlgItem(IDC_STATIC_CON_STATE);
	m_edit_user_id = (CEdit*)GetDlgItem(IDC_EDIT_USER_ID);
	m_edit_password = (CEdit*)GetDlgItem(IDC_EDIT_PASSWORD);
	m_btn_start = (CButton*)GetDlgItem(IDC_BUTTON_START);

	TCHAR tcPath[MAX_PATH] = _T("account.txt");
	char *cPath;
	TCHAR lpTempPathBuffer[MAX_PATH];
	DWORD dwRetVal = 0;
	dwRetVal = GetTempPath(MAX_PATH,          // length of the buffer
		lpTempPathBuffer); // buffer for path 
	USES_CONVERSION;
	if (dwRetVal > MAX_PATH || (dwRetVal == 0)) {
		cPath = W2A(tcPath);
	}
	else {
		_tcscat(lpTempPathBuffer, tcPath);
		cPath = W2A(lpTempPathBuffer);
	}
	string userid, password;
	ifstream accountfile(cPath);
	if (accountfile.is_open())
	{
		getline(accountfile, userid);
		getline(accountfile, password);
		accountfile.close();
		m_edit_user_id->SetWindowText(CString(userid.c_str()));
		m_edit_password->SetWindowText(CString(password.c_str()));
		((CButton*)GetDlgItem(IDC_CHECK_REMEMBER))->SetCheck(1);
	}
	
	LVCOLUMN lvColumn;
	lvColumn.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvColumn.fmt = LVCFMT_LEFT;
	lvColumn.cx = 200;
	lvColumn.pszText = L"Description";
	m_list_forums->InsertColumn(0, &lvColumn);
	m_list_timers->InsertColumn(0, &lvColumn);

	lvColumn.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvColumn.fmt = LVCFMT_LEFT;
	lvColumn.cx = 100;
	lvColumn.pszText = L"Ships Wanted";
	m_list_forums->InsertColumn(1, &lvColumn);

	lvColumn.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvColumn.fmt = LVCFMT_LEFT;
	lvColumn.cx = 600;
	lvColumn.pszText = L"Time Left";
	m_list_forums->InsertColumn(2, &lvColumn);
	m_list_timers->InsertColumn(1, &lvColumn);	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CRazorAllianeTimerGrabDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CRazorAllianeTimerGrabDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CRazorAllianeTimerGrabDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CRazorAllianeTimerGrabDlg::OnDestroy()
{
	CDialog::OnDestroy();
	// TODO: Add your message handler code here
	KillTimer(1);
}

void CRazorAllianeTimerGrabDlg::replaceAll(std::string& str, const std::string& from, const std::string& to) {
	if (from.empty())
		return;
	size_t start_pos = 0;
	while ((start_pos = str.find(from, start_pos)) != std::string::npos) {
		str.replace(start_pos, from.length(), to);
		start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
	}
}

wstring CRazorAllianeTimerGrabDlg::s2ws(const std::string& s)
{
	int len;
	int slength = (int)s.length() + 1;
	len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
	wchar_t* buf = new wchar_t[len];
	MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
	std::wstring r(buf);
	delete[] buf;
	return r;
}

time_t CRazorAllianeTimerGrabDlg::getEpochTime(const std::string& dateTime)
{
	// Let's consider we are getting all the input in
	// this format: '2014-07-25T20:17:22Z' (T denotes
	// start of Time part, Z denotes UTC zone).
	// A better approach would be to pass in the format as well.
	static const std::wstring dateTimeFormat{ L"%Y-%m-%d %H:%M:%S" };

	// Create a stream which we will use to parse the string,
	// which we provide to constructor of stream to fill the buffer.
	const std::wstring wdateTime(dateTime.begin(), dateTime.end());
	std::wistringstream ss{ wdateTime };

	// Create a tm object to store the parsed date and time.
	std::tm dt;

	// Now we read from buffer using get_time manipulator
	// and formatting the input appropriately.
	ss >> std::get_time(&dt, dateTimeFormat.c_str());

	// Convert the tm structure to time_t value and return.
	return std::mktime(&dt);
}

void CRazorAllianeTimerGrabDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default
		char *buffer = NULL;
		if (!m_mt) return;
		if (!m_mt->m_is_fetching_success) return;
		vector<CaptureData>	cd = m_mt->m_cd;
		time_t nowLocal = time(NULL);
		struct tm *tmp = gmtime(&nowLocal);
		time_t now = mktime(tmp);
		for (std::vector<CaptureData>::iterator it = cd.begin(); it != cd.end(); ++it) {
			if (it->isIn == false) continue;
			std::string strDesc = it->strDesc;
			USES_CONVERSION_EX;
			LPWSTR lp = A2W_EX(strDesc.c_str(), strDesc.length());
			LVFINDINFO info;
			info.flags = LVFI_PARTIAL | LVFI_STRING;
			info.psz = lp;
			int nItem;
			time_t rawTimeStart;			
			rawTimeStart = getEpochTime(it->strDate);
			double diffSeconds = difftime(rawTimeStart, now);
			int day = (int)diffSeconds / (24 * 60 * 60);
			diffSeconds -= day * 24 * 60 * 60;
			int hour = (int)diffSeconds / (60 * 60);
			diffSeconds -= hour * 60 * 60;
			int minu = (int)diffSeconds / 60;
			int sec = (int)diffSeconds % 60;
			if (buffer) {
				delete buffer;
				buffer = NULL;
			}
			buffer = new char[50];
			sprintf(buffer, "%3dD-%2dH-%2dM-%2dS", day, hour, minu, sec);
			string strDiff = string(buffer);
			replaceAll(strDiff, " ", "0");
			replaceAll(strDiff, "-", " ");

			if (it->isOperation) {
				if ((nItem = m_list_forums->FindItem(&info)) != -1) {
					m_list_forums->SetItemText(nItem, 2, (s2ws(strDiff)).c_str());
				}
			}
			else {
				if ((nItem = m_list_timers->FindItem(&info)) != -1) {
					m_list_timers->SetItemText(nItem, 1, (s2ws(strDiff)).c_str());
				}
			}
		}
		if (buffer) delete buffer;
	CDialog::OnTimer(nIDEvent);
}

void CRazorAllianeTimerGrabDlg::OnBnClickedButtonStart()
{
	// TODO: Add your control notification handler code here
	CString strCaption;
	m_btn_start->GetWindowText(strCaption);
	if (strCaption == "Start") {
		m_btn_start->SetWindowText(L"Stop");		
	}else {
		m_btn_start->SetWindowText(L"Start");
		if (m_mt) {
			delete m_mt;
			m_mt = NULL;
		}
		m_static_con_state->SetWindowText(L"Stoped. To Start Click Start Button");
		m_list_forums->DeleteAllItems();
		m_list_timers->DeleteAllItems();
		return;
	}
	if (m_mt) {
		delete m_mt;
		m_mt = NULL;
	}	
	if (m_ts) {
		delete m_ts;
		m_ts = NULL;
	}
	m_ts = new THREADSTRUCT;
	m_ts->list_forums = m_list_forums;
	m_ts->list_timers = m_list_timers;
	m_ts->edit_user_id = m_edit_user_id;
	m_ts->edit_password = m_edit_password;
	m_ts->static_con_state = m_static_con_state;
	m_ts->btn_start = m_btn_start;

	m_mt = new MainThread(m_ts);
	m_mt->Create(0);
	SetTimer(1, 1000, NULL);
}

void CRazorAllianeTimerGrabDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);
	RECT rt;
	//m_edit_password->SetWindowPos(NULL,0,0,cx)
	//int x = 10;
	// TODO: Add your message handler code here
}


void CRazorAllianeTimerGrabDlg::OnBnClickedCheckRemember()
{
	// TODO: Add your control notification handler code here
	bool ischeck = ((CButton*)GetDlgItem(IDC_CHECK_REMEMBER))->GetCheck();
	TCHAR tcPath[MAX_PATH] = _T("account.txt");
	char *cPath;
	TCHAR lpTempPathBuffer[MAX_PATH];
	DWORD dwRetVal = 0; 
	dwRetVal = GetTempPath(MAX_PATH,          // length of the buffer
		lpTempPathBuffer); // buffer for path 
	USES_CONVERSION;
	if (dwRetVal > MAX_PATH || (dwRetVal == 0)){
		cPath = W2A(tcPath);
	}
	else {
		_tcscat(lpTempPathBuffer, tcPath);
		cPath = W2A(lpTempPathBuffer);
	}
	if (ischeck) {
		ofstream accountfile(cPath);
		if (accountfile.is_open())
		{
			CString strUserID, strPassword;			
			m_edit_user_id->GetWindowText(strUserID);
			m_edit_password->GetWindowText(strPassword);
			TCHAR   tcUserID[MAX_PATH], tcPassword[MAX_PATH];
			_tcscpy(tcUserID, strUserID.GetBuffer(MAX_PATH));
			_tcscpy(tcPassword, strPassword.GetBuffer(MAX_PATH));
			char*	cUserID,* cPassword;
			cUserID = W2A(tcUserID);
			cPassword = W2A(tcPassword);
			accountfile << cUserID << "\n";
			accountfile << cPassword << "\n";
			accountfile.close();
		}
		else AfxMessageBox(_T("Unable To Open File."));
	}
	else {
		remove(cPath);
	}
}
