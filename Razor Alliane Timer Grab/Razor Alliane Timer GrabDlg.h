// Razor Alliane Timer GrabDlg.h : header file
//

#pragma once

#include "MainThread.h"

// CRazorAllianeTimerGrabDlg dialog
class CRazorAllianeTimerGrabDlg : public CDialog
{
// Construction
public:
	CRazorAllianeTimerGrabDlg(CWnd* pParent = NULL);	// standard constructor
	~CRazorAllianeTimerGrabDlg();	// standard destructor
// Dialog Data
	enum { IDD = IDD_RAZORALLIANETIMERGRAB_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT_PTR nIDEvent);

public:
	THREADSTRUCT *m_ts;
public:
	CListCtrl	*m_list_forums;
	CListCtrl	*m_list_timers;
	CStatic		*m_static_con_state;
	CEdit		*m_edit_user_id;
	CEdit		*m_edit_password;
	CButton		*m_btn_start;
	MainThread	*m_mt;

public:
	afx_msg void OnBnClickedButtonStart();
	void replaceAll(std::string& str, const std::string& from, const std::string& to);
	wstring s2ws(const std::string& s);
	time_t getEpochTime(const std::string& dateTime);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnBnClickedCheckRemember();
};