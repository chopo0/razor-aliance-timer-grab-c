#include "stdafx.h"
#include "MainThread.h"

MainThread::MainThread(THREADSTRUCT * ts)
{
	m_Thread = NULL;

	m_strCookie = "";
	m_strRefer = "";
	
	m_edit_user_id = ts->edit_user_id;
	m_edit_password = ts->edit_password;
	m_list_forums = ts->list_forums;
	m_list_timers = ts->list_timers;
	m_static_con_state = ts->static_con_state;
	m_btn_start = ts->btn_start;
	m_cd.clear();
	m_is_fetching_success = false;
}

MainThread::~MainThread()
{
	DWORD exitCode;
	if (m_Thread != NULL && ::GetExitCodeThread(m_Thread->m_hThread, &exitCode) && exitCode == STILL_ACTIVE)
		Destroy();
}

void MainThread::Create(int mode )
{
	if (!m_Thread) {
		// Auto reset, initially reset
		EKillThread = ::CreateEvent(NULL,FALSE,FALSE,NULL);
		EThreadIsDead = ::CreateEvent(NULL,FALSE,FALSE,NULL);
		// Create the thread.
		m_Thread = ::AfxBeginThread(ThreadFunc,this,THREAD_PRIORITY_NORMAL,0);
	}
}

void MainThread::Destroy(void)
{
	if (m_Thread) {
		// Tell thread to commit suicide
		BOOL retcode = ::SetEvent(EKillThread);
		if (!retcode)
			TRACE0("SetEvent(EKillThread) in TBaseThread::Destroy() returned error\n");
		// Wait for info that thread is killed
		//if (::WaitForSingleObject(EThreadIsDead, 10000L) == WAIT_TIMEOUT) {
			// Safe guard - if something is wrong then brutally kill thread
			::TerminateThread(m_Thread->m_hThread, 1);
		//}
		// Wait a bit
		::Sleep(10);
		// Maybe not necessary, but just in case
		m_Thread = NULL;
	}
	::CloseHandle(EKillThread);
	::CloseHandle(EThreadIsDead);
}

UINT MainThread::ThreadFunc(LPVOID arg)
{
	MainThread *thread = (MainThread*)arg;
	thread->DoExecute();
	return 0;
}

void MainThread::DoExecute(void)
{
	Execute(); // Abstract virtual function
	::SetEvent(EThreadIsDead);
}

void MainThread::Suspend(void)
{
	if (m_Thread)
		m_Thread->SuspendThread();
}

void MainThread::Resume(void)
{
	if (m_Thread)
		m_Thread->ResumeThread();
}

void MainThread::Execute(void)
{
	DWORD retcode;
	while (TRUE) {
		// EKillThread is a protected data member of TBaseThread.
		retcode = ::WaitForSingleObject(EKillThread, 100L);
		if (retcode == WAIT_OBJECT_0) {
			break; // Thread owner signaled this thread to exit
		}
		else if (retcode == WAIT_TIMEOUT) {
			// Process data - thread specific code
			//see the access mode to your dialog controls
			CString strUserId, strPassword;
			m_edit_user_id->GetWindowText(strUserId);
			m_edit_password->GetWindowText(strPassword);
			CT2CA pszConvertedAnsiString(strUserId);
			m_strUser = string(pszConvertedAnsiString);

			CT2CA pszConvertedAnsiString1(strPassword);
			m_strPasswrd = string(pszConvertedAnsiString1);

			m_strHost = "www.eve-razor.com";
			m_strIndexPath = "";
			m_static_con_state->SetWindowText(L"Connecting...");
			int nTryCount = 10;
			while (m_strIndexPath == "") {
				nTryCount--;
				m_strCookie = "";
				if (nTryCount < 0) {
					m_static_con_state->SetWindowText(L"Connection Failed. Check your account and Network Connection!");
					m_btn_start->SetWindowText(L"Start");
					return;
				}
				connectionEstablish();
			}
			m_static_con_state->SetWindowText(L"Connection Established");
			Sleep(1000);
			while(1){
				fetching();
				Sleep(2300);
			}
		}
	}
}

void MainThread::fetching() {
	char *buffer = NULL;
	m_static_con_state->SetWindowText(L"Trying to Fetch Data...");
	string res_forum = HTTP_GET(m_strHost, m_strIndexPath);
	replaceAll(res_forum, "\r", "");
	replaceAll(res_forum, "\n", "");
	int nPos = res_forum.find("<tr class=\"windowbg2\"><td class=\"timer_h\">", 0);
	if (nPos == -1) {
		m_static_con_state->SetWindowText(L"Fetching Erro!!!");
		return;
	}
	CaptureData cd;
	for (std::vector<CaptureData>::iterator it = m_cd.begin(); it != m_cd.end(); ++it) {
		it->isIn = false;
	}
	while (nPos != -1) {
		bool isOperation = true;
		int nPosStart = res_forum.find("<a href=\"\">", nPos) + 11;
		if ((nPosStart - nPos) != 53) {
			nPosStart = res_forum.find("<a href=\"#\">", nPos) + 12;
			if ((nPosStart - nPos) != 54) {
				break;
			}
			isOperation = false;
		}
		cd.isOperation = isOperation;
		cd.isIn = false;
		int nPosEnd = res_forum.find("</a>", nPosStart);
		cd.strDesc = res_forum.substr(nPosStart, nPosEnd - nPosStart);
		int nPosTd = nPosEnd;
		int nTdIndex = 0;
		time_t rawTimeStart;
		time_t nowLocal = time(NULL);
		struct tm *tmp = gmtime(&nowLocal);
		time_t now = mktime(tmp);
		double diffSeconds;
		while (nTdIndex < 5)
		{
			int nPosTdStart = res_forum.find("<td>", nPosTd) + 4;
			int nPosTdEnd = res_forum.find("</td>", nPosTdStart);
			string strTd = res_forum.substr(nPosTdStart, nPosTdEnd - nPosTdStart);
			switch (nTdIndex)
			{
			case 0:
				cd.strFC = strTd;
				break;
			case 1:
				cd.strType = strTd;
				break;
			case 2:
				cd.strStartSystem = strTd;
				break;
			case 3:
				cd.strShip = strTd;
				break;
			case 4: {
				int nPosDateTdEnd = strTd.find("(", 0) - 1;
				cd.strDate = strTd.substr(0, nPosDateTdEnd) + ":00";
				rawTimeStart = getEpochTime(cd.strDate);
				diffSeconds = difftime(rawTimeStart, now);
				int day = (int)diffSeconds / (24 * 60 * 60);
				diffSeconds -= day * 24 * 60 * 60;
				int hour = (int)diffSeconds / (60 * 60);
				diffSeconds -= hour * 60 * 60;
				int minu = (int)diffSeconds / 60;
				int sec = (int)diffSeconds % 60;
				if (buffer) {
					delete buffer;
					buffer = NULL;
				}
				buffer = new char[50];
				sprintf(buffer, "%3dD-%2dH-%2dM-%2dS", day, hour, minu, sec);
				cd.strDiff = string(buffer);
				replaceAll(cd.strDiff, " ", "0");
				replaceAll(cd.strDiff, "-", " ");
				break;
			}
			default:
				break;
			}
			nTdIndex++;
			nPosTd = nPosTdEnd;
		}

		nPos = res_forum.find("<tr class=\"windowbg2\"><td class=\"timer_h\">", nPosEnd);

		USES_CONVERSION_EX;
		std::string strDesc = cd.strDesc;
		LPWSTR lp = A2W_EX(strDesc.c_str(), strDesc.length());
		LVFINDINFO info;
		info.flags = LVFI_PARTIAL | LVFI_STRING;
		info.psz = lp;
		int nItem;


		bool flag = false;
		std::vector<CaptureData>::iterator findItr;
		for (std::vector<CaptureData>::iterator itemItr = m_cd.begin(); itemItr != m_cd.end(); ++itemItr) {
			if ((itemItr->strDesc == cd.strDesc) && (itemItr->strDate == cd.strDate) && (itemItr->isOperation == cd.isOperation) &&
				(itemItr->strFC == cd.strFC) && (itemItr->strShip == cd.strShip) && (itemItr->strStartSystem == cd.strStartSystem) &&
				(itemItr->strType == cd.strType)) {
				itemItr->isIn = true;
				findItr = itemItr;
				flag = true;
				break;
			}
		}

		if (isOperation) {
			nItem = m_list_forums->FindItem(&info);
			if (flag) {
				m_list_forums->SetItemText(nItem, 2, (s2ws(cd.strDiff)).c_str());
			}
			else {
				LVITEM lvItem;
				lvItem.mask = LVIF_TEXT;
				lvItem.iItem = 0;
				lvItem.iSubItem = 0;
				lvItem.pszText = lp;

				nItem = m_list_forums->InsertItem(&lvItem);
				m_list_forums->SetItemText(nItem, 1, (s2ws(cd.strShip)).c_str());
				m_list_forums->SetItemText(nItem, 2, (s2ws(cd.strDiff)).c_str());
				cd.strDiff = "";
				cd.isIn = true;
				m_cd.push_back(cd);
			}
		}
		else {
			nItem = m_list_timers->FindItem(&info);
			if (flag) {
				m_list_timers->SetItemText(nItem, 1, (s2ws(cd.strDiff)).c_str());
			}
			else {
				LVITEM lvItem;
				lvItem.mask = LVIF_TEXT;
				lvItem.iItem = 0;
				lvItem.iSubItem = 0;
				lvItem.pszText = lp;

				nItem = m_list_timers->InsertItem(&lvItem);
				m_list_timers->SetItemText(nItem, 1, (s2ws(cd.strDiff)).c_str());
				cd.strDiff = "";
				cd.isIn = true;
				m_cd.push_back(cd);
			}
		}
	}
	for (std::vector<CaptureData>::iterator it = m_cd.begin(); it != m_cd.end(); true) {
		if (it->isIn == true) {
			it++;
			continue;
		}
		USES_CONVERSION_EX;
		std::string strDesc = it->strDesc;
		LPWSTR lp = A2W_EX(strDesc.c_str(), strDesc.length());
		LVFINDINFO info;
		info.flags = LVFI_PARTIAL | LVFI_STRING;
		info.psz = lp;
		int nItem;
		if (it->isOperation) {
			nItem = m_list_forums->FindItem(&info);
			if (nItem != -1) {
				m_list_forums->DeleteItem(nItem);
			}
		}
		else {
			nItem = m_list_timers->FindItem(&info);
			if (nItem != -1) {
				m_list_timers->DeleteItem(nItem);
			}
		}
		it = m_cd.erase(it);

	}
	m_static_con_state->SetWindowText(L"Fetching Success");
	m_is_fetching_success = true;
}
void MainThread::connectionEstablish()
{
	/* Getting Data on Login Page */
	string  host = "www.eve-razor.com",
		path = "/forumv2/index.php?action=login";
	m_strRefer = "";
	string res = HTTP_GET(host, path,false);
	int nPos = res.find("hashLoginPassword", 0);	
	if (nPos == -1) {
		m_static_con_state->SetWindowText(L"Connection Failed. Retrying to Fetching Indx Html");
		return;
	}
	m_static_con_state->SetWindowText(L"Fetching Indx Html Success");
	int nPosStart = res.find("'", nPos) + 1;
	int nPosEnd = res.find("'", nPosStart);
	string cur_session_id = res.substr(nPosStart, nPosEnd - nPosStart);

	/* Getting Param on Data */
	string strUser = m_strUser;
	transform(strUser.begin(), strUser.end(), strUser.begin(), ::tolower);
	transform(m_strPasswrd.begin(), m_strPasswrd.end(), m_strPasswrd.begin(), ::tolower);
	string strHexSha1Key = strUser + m_strPasswrd;
	unsigned char cHexSha1Key[300];
	copy(strHexSha1Key.begin(), strHexSha1Key.end(), cHexSha1Key);
	cHexSha1Key[strHexSha1Key.length()] = 0;
	unsigned char hash[SHA_DIGEST_LENGTH]; // == 20
	SHA1(cHexSha1Key, strHexSha1Key.length(), hash);
	string strHexSha1LeftKey = hex_hash(hash);
	transform(strHexSha1LeftKey.begin(), strHexSha1LeftKey.end(), strHexSha1LeftKey.begin(), ::tolower);
	string strHexSha1KeyFinal = strHexSha1LeftKey + cur_session_id;
	unsigned char cHexSha1KeyFinal[500];
	copy(strHexSha1KeyFinal.begin(), strHexSha1KeyFinal.end(), cHexSha1KeyFinal);

	//m_static_con_state->SetWindowText(CString(res.c_str())); //Error Occured

	cHexSha1KeyFinal[strHexSha1KeyFinal.length()] = 0;
	unsigned char hash1[SHA_DIGEST_LENGTH]; // == 20
	SHA1(cHexSha1KeyFinal, strHexSha1KeyFinal.length(), hash1);
	//m_static_con_state->SetWindowText(L"cur5");
	string hash_passwrd = hex_hash(hash1);
	transform(hash_passwrd.begin(), hash_passwrd.end(), hash_passwrd.begin(), ::tolower);

	nPos = res.find("PHPSESSID", 0);
	nPosStart = res.find("=", nPos) + 1;
	nPosEnd = res.find(";", nPosStart);
	m_strCookie = "Cookie: PHPSESSID=" + res.substr(nPosStart, nPosEnd - nPosStart);

	/* Login Post Login2 action */
	m_static_con_state->SetWindowText(L"Login to Server");

	host = "www.eve-razor.com";
	path = "/forumv2/index.php?action=login2";
	string data = "user=" + m_strUser + "&passwrd=&cookieneverexp=on&hash_passwrd=" + hash_passwrd;
	string res_login2_post = HTTP_POST(host, path, data);

	/* Login Get Login2 action */
	nPos = res_login2_post.find("Location", 0);
	if (nPos == -1) {
		m_static_con_state->SetWindowText(L"Login Failed, your account is invalid");
		return;
	}
	nPosStart = res_login2_post.find("/forumv2/", nPos);
	if (nPos == -1) {
		m_static_con_state->SetWindowText(L"Login Failed, Connection Problem");
		return;
	}
	nPosEnd = res_login2_post.find("\r", nPosStart);
	path = res_login2_post.substr(nPosStart, nPosEnd - nPosStart);

	nPosStart = res_login2_post.find("RZRCookie666=", 0);
	nPosEnd = res_login2_post.find("\r", nPosStart);
	string RZRCookie666 = res_login2_post.substr(nPosStart, nPosEnd - nPosStart);

	nPosStart = res_login2_post.find("PHPSESSID=", 0);
	nPosEnd = res_login2_post.find("\r", nPosStart);
	string PHPSESSID = res_login2_post.substr(nPosStart, nPosEnd - nPosStart);

	//m_strRefer = "Referer: https://www.eve-razor.com/forumv2/index.php?action=login";
	m_strCookie = "Cookie: " + RZRCookie666 + "; " + PHPSESSID;
	string res_login2_get = HTTP_GET(host, path, true);

	/* Get Forum URL */
	nPos = res_login2_get.find("Location", 0);
	if (nPos == -1) {
		m_static_con_state->SetWindowText(L"Login Failed, Connection Problem");
		return;
	}
	nPosStart = res_login2_get.find("/forumv2/", nPos);
	if (nPosStart == -1) {
		m_static_con_state->SetWindowText(L"Login Failed, Connection Problem");
		return;
	}
	nPosEnd = res_login2_get.find("\r", nPosStart);
	path = res_login2_get.substr(nPosStart, nPosEnd - nPosStart);
	m_strIndexPath = path;
	//m_strRefer = "Referer: https://www.eve-razor.com/forumv2/index.php?action=login";
}

string MainThread::HTTP_GET(string host, string path, bool quick) {
	string request;
	string response;
	int resp_leng;

	char buffer[BUFFERSIZE];
	struct sockaddr_in serveraddr;
	int sock;

	WSADATA wsaData;
	int port = 80;

	struct hostent *hp;
	const char* host_name = host.c_str();

	std::stringstream ss;

	std::stringstream request2;

	request2 << "GET " << path << " HTTP/1.1" << endl;
	request2 << "User-Agent:Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36" << endl;
	//request2 << "" << endl;
	request2 << "Host: " << host << endl;
	if (m_strCookie != "") request2 << m_strCookie << endl;
	if (m_strRefer != "") request2 << m_strRefer << endl;
	request2 << endl;
	request = request2.str();
	//init winsock
	if (WSAStartup(MAKEWORD(2, 0), &wsaData) != 0){
		die_with_wserror("WSAStartup() failed");
		return "";
	}
	//open socket
	if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		return "";
	}
	//connect
	if ((hp = gethostbyname(host_name)) == NULL) {
		return "";
	}
	//bcopy(hp->h_addr, &addr.sin_addr, hp->h_length);
	memset(&serveraddr, 0, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = *((unsigned long*)hp->h_addr);
	serveraddr.sin_port = htons((unsigned short)port);
	if (connect(sock, (struct sockaddr *) &serveraddr, sizeof(serveraddr)) < 0)
		die_with_wserror("connect() failed");

	//send request
	if (send(sock, request.c_str(), request.length(), 0) != request.length())
		die_with_wserror("send() sent a different number of bytes than expected");

	//get response
	response = "";
	resp_leng = BUFFERSIZE;

	//make socket non blocking

	//beginning time
	time_t begin = time(NULL);
	int total_size = 0;
	if (!quick) {
		while (1)
		{
			time_t now = time(NULL);
			double diffSeconds = difftime(now, begin);

			if (total_size > 0 && diffSeconds > TIMEOUT)
			{
				break;
			}
			else if (diffSeconds > TIMEOUT * 2) {
				break;
			}
			memset(buffer, 0, BUFFERSIZE);
			resp_leng = recv(sock, (char*)&buffer, BUFFERSIZE, 0);
			if (resp_leng >= 0) {
				response += string(buffer).substr(0, resp_leng);
				total_size += resp_leng;
			}
			//note: download lag is not handled in this code
		}
	}else{
		while (resp_leng == BUFFERSIZE)
		{
			resp_leng = recv(sock, (char*)&buffer, BUFFERSIZE, 0);
			if (resp_leng>0)
				response += string(buffer).substr(0, resp_leng);
			//note: download lag is not handled in this code
		}
	}
	//disconnect
	closesocket(sock);

	//cleanup
	WSACleanup();

	return  response;
}

string MainThread::HTTP_POST(string host, string path, string data) {
	string request;
	string response;
	int resp_leng;

	char buffer[BUFFERSIZE];
	struct sockaddr_in serveraddr;
	int sock;

	WSADATA wsaData;
	int port = 80;
	struct hostent *hp;
	const char* host_name = host.c_str();

	std::stringstream request2;

	request2 << "POST " << path << " HTTP/1.1" << endl;
	request2 << "User-Agent:Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36" << endl;
	//request2 << "" << endl;
	request2 << "Host: " << host << endl;
	request2 << "Content-Length: " << data.length() << endl;

	request2 << "Content-Type: application/x-www-form-urlencoded" << endl;
	if (m_strCookie != "") request2 << m_strCookie << endl;
	request2 << "Accept-Language: en-US" << endl;
	request2 << endl;
	request2 << data;
	request = request2.str();

	//init winsock
	if (WSAStartup(MAKEWORD(2, 0), &wsaData) != 0)
		die_with_wserror("WSAStartup() failed");
	//open socket
	if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
		die_with_wserror("socket() failed");

	//connect
	if ((hp = gethostbyname(host_name)) == NULL) {
		exit(1);
	}
	memset(&serveraddr, 0, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = *((unsigned long*)hp->h_addr);
	serveraddr.sin_port = htons((unsigned short)port);
	if (connect(sock, (struct sockaddr *) &serveraddr, sizeof(serveraddr)) < 0)
		die_with_wserror("connect() failed");

	//send request
	if (send(sock, request.c_str(), request.length(), 0) != request.length())
		die_with_wserror("send() sent a different number of bytes than expected");

	//get response
	response = "";
	resp_leng = BUFFERSIZE;
	while (resp_leng == BUFFERSIZE)
	{
		resp_leng = recv(sock, (char*)&buffer, BUFFERSIZE, 0);
		if (resp_leng>0)
			response += string(buffer).substr(0, resp_leng);
		//note: download lag is not handled in this code
	}

	//disconnect
	closesocket(sock);

	//cleanup
	WSACleanup();

	return  response;
}

void MainThread::die_with_error(char *errorMessage)
{
	cerr << errorMessage << endl;
	cin.get();
	exit(1);
}

void MainThread::die_with_wserror(char *errorMessage)
{
	cerr << errorMessage << ": " << WSAGetLastError() << endl;
	cin.get();
	exit(1);
}

string MainThread::hex_hash(unsigned char *hash) {
	if (hash == NULL) {
		return "";
	}
	string result = "";
	char buffer[12];
	for (int i = 0; i < SHA_DIGEST_LENGTH; i++) {
		sprintf(buffer, "%02X", hash[i]);
		result += buffer;
	}
	return result;
}

void MainThread::replaceAll(std::string& str, const std::string& from, const std::string& to) {
	if (from.empty())
		return;
	size_t start_pos = 0;
	while ((start_pos = str.find(from, start_pos)) != std::string::npos) {
		str.replace(start_pos, from.length(), to);
		start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
	}
}

wstring MainThread::s2ws(const std::string& s)
{
	int len;
	int slength = (int)s.length() + 1;
	len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
	wchar_t* buf = new wchar_t[len];
	MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
	std::wstring r(buf);
	delete[] buf;
	return r;
}

time_t MainThread::getEpochTime(const std::string& dateTime)
{
	// Let's consider we are getting all the input in
	// this format: '2014-07-25T20:17:22Z' (T denotes
	// start of Time part, Z denotes UTC zone).
	// A better approach would be to pass in the format as well.
	static const std::wstring dateTimeFormat{ L"%Y-%m-%d %H:%M:%S" };

	// Create a stream which we will use to parse the string,
	// which we provide to constructor of stream to fill the buffer.
	const std::wstring wdateTime(dateTime.begin(), dateTime.end());
	std::wistringstream ss{ wdateTime };

	// Create a tm object to store the parsed date and time.
	std::tm dt;

	// Now we read from buffer using get_time manipulator
	// and formatting the input appropriately.
	ss >> std::get_time(&dt, dateTimeFormat.c_str());

	// Convert the tm structure to time_t value and return.
	return std::mktime(&dt);
}