//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Razor Alliane Timer Grab.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_RAZORALLIANETIMERGRAB_DIALOG 102
#define IDR_MAINFRAME                   128
#define IDI_ICON                        139
#define IDI_ICON_MAIN                   139
#define IDC_LIST_FORUMS                 1002
#define IDC_STATIC_CON_STATE            1003
#define IDC_LIST_TIMERS                 1004
#define IDC_BUTTON_START                1005
#define IDC_EDIT_USER_ID                1006
#define IDC_EDIT_PASSWORD               1007
#define IDC_CHECK_REMEMBER              1011

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        140
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1012
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
