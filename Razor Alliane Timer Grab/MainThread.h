#pragma once

#include <string>
#include <iostream>
#include <fstream>
#pragma comment (lib, "wsock32.lib")

#include <stdlib.h>
#include <winsock.h>
#include <sstream>
#include <ws2tcpip.h>
#include <vector>
#include "windows.h"
#include <openssl/sha.h>
#include <algorithm>
#include <iostream>
#include <thread>
#include <ctime>
#include <iomanip>
#include <fcntl.h> //fcntl

#define BUFFERSIZE 1024
#define TIMEOUT 1

using namespace std;

struct CaptureData
{
	string strDesc;
	string strFC;
	string strType;
	string strStartSystem;
	string strShip;
	string strDate;
	string strDiff;
	bool   isOperation;
	bool   isIn;
};

//structure for passing to the controlling function
struct THREADSTRUCT
{
	CListCtrl	*list_forums;
	CListCtrl	*list_timers;
	CStatic		*static_con_state;
	CEdit		*edit_user_id;
	CEdit		*edit_password;
	CButton		*btn_start;
};
class MainThread
{
public:
	MainThread(THREADSTRUCT * ts);
	~MainThread();
protected:
	HANDLE EKillThread; // When set, thread will be killed
	HANDLE EThreadIsDead; // Thread sets to indicate thread is finished
private:
	CWinThread *m_Thread;

public:
	static UINT __cdecl ThreadFunc(LPVOID);

	virtual void Create(int mode = 0);
	virtual void Destroy(void);
	void DoExecute(void);
	void Execute(void);
	void Suspend(void);
	void Resume(void);

public:
	string		m_strCookie;
	string		m_strRefer;
	string		m_strUser;
	string		m_strPasswrd;
	string		m_strIndexPath;
	string		m_strHost;
	vector<CaptureData>		m_cd;
	bool		m_is_fetching_success;
public:
	CListCtrl	*m_list_forums;
	CListCtrl	*m_list_timers;
	CStatic		*m_static_con_state;
	CEdit		*m_edit_user_id;
	CEdit		*m_edit_password;
	CButton		*m_btn_start;

public:
	string HTTP_POST(string host, string path, string data);
	string HTTP_GET(string host, string path, bool quick = false);
	void die_with_error(char *errorMessage);
	void die_with_wserror(char *errorMessage);
	void connectionEstablish();
	string hex_hash(unsigned char* hash);
	void replaceAll(std::string& str, const std::string& from, const std::string& to);
	wstring s2ws(const std::string& s);
	time_t getEpochTime(const std::string& dateTime);
	void fetching();
};

